using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Thien_Demo
{ 
    public class CharacterController : MonoBehaviour
    {
        public GameData gameData;

        public MapData mapData;
        
        public CharacterData characterData;
        
        public SpriteRenderer visualSprite;

        public Transform currentHpSprite;

        private int _currentHP;

        void Start()
        {
            _currentHP = characterData.defaultHP;
            
            currentHpSprite.localScale = Vector3.one;
            
            visualSprite.sprite = characterData.visual;

            var localPosition = transform.localPosition;
            var x = Mathf.CeilToInt(localPosition.x / mapData.widthSize);
            var y = Mathf.CeilToInt(localPosition.y / mapData.heightSize);
            _position = new Vector2Int(x, y);

        }

        public bool IsMove => characterData.isMove;
        public bool IsAttack => characterData.attacker;

        private Vector2Int _position;
        public Vector2Int Position => _position;
        
        public void SetPosition(Vector2Int newPosition)
        {
            _position = newPosition;
            
            var newLocalPosition = new Vector3(newPosition.x * mapData.widthSize, newPosition.y * mapData.heightSize);
            transform.DOLocalMove(newLocalPosition, GameManager.Instance.TurnDuration / 2);
        }

        private int _randomNum = 0;
        public int RandomNumber => _randomNum;
        
        public void RefreshRandomNumber()
        {
            _randomNum = Random.Range(0, gameData.DamageCount);
        }
        
        public int DealDamage(int targetNumber)
        {
            var damageCount = gameData.DamageCount;
            var point = (damageCount + _randomNum - targetNumber) % damageCount;
            return gameData.GetDamage(point);
        }

        public void TakeDamage(int damage)
        {
            if (damage <= 0) return;

            _currentHP = Mathf.Max(0, _currentHP - damage);

            currentHpSprite.DOScaleX((float) _currentHP / characterData.defaultHP, GameManager.Instance.TurnDuration / 2)
                .OnComplete(() =>
                {
                    if (_currentHP <= 0)
                    {
                        Destroy(this.gameObject);
                    }
                });
        }
    }   
}
