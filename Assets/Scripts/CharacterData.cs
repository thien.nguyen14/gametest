using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Character Data", menuName = "GameAsset/CharacterData", order = 0)]
public class CharacterData : ScriptableObject
{
    public Sprite visual;
    public bool isMove;
    public bool attacker;
    public int defaultHP;
}
