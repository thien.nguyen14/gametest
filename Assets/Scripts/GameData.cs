using UnityEngine;
using System.Collections.Generic;
using System;

[CreateAssetMenu(fileName = "Game Data", menuName = "GameAsset/GameData", order = 20)]
public class GameData : ScriptableObject
{
    private Dictionary<int, int> damageDictionary = new Dictionary<int, int>
    {
        {0, 4},
        {1, 5},
        {2, 3},
    };

    public int DamageCount => damageDictionary.Count;

    public int turnDuration;

    public int GetDamage(int point)
    {
        if (damageDictionary == null || damageDictionary.Count == 0) return 0;
        
        return damageDictionary.ContainsKey(point) ? damageDictionary[point] : damageDictionary[0];
    }
}             