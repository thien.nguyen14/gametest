using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Thien_Demo
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;
        
        [Header("Data")]
        public GameData gameData;

        [Header("GamePLay")] 
        public GameObject attackTeamPrefab;
        public GameObject defendTeamPrefab;

        [Header("Result")] 
        public Text resultDescription;
        public GameObject resultPanel;
        
        private bool _isPause = false;

        private int _speed = 1;
        
        private GameObject attackTeam;
        private GameObject defendTeam; 

        public float TurnDuration=> (float) gameData.turnDuration / _speed;
        
        // Start is called before the first frame update
        void Start()
        {
            Instance = this;

            Init();
        }

        private void Init()
        {
            if (attackTeam != null) Destroy(attackTeam.gameObject);
            attackTeam = Instantiate(attackTeamPrefab);
            
            if (defendTeam != null) Destroy(defendTeam.gameObject);
            defendTeam = Instantiate(defendTeamPrefab);
            
            _isPause = false;
            pauseButtonTxt.text = "Pause";
            
            _speed = 1;
            speedButtonTxt.text = "x1";
            
            resultPanel.SetActive(false);
            
            StartCoroutine(RunGame());
        }
        
        #region GAME_PLAY

        private List<CharacterController> listAttackCharacter;
        private List<CharacterController> listDefendCharacter;

        IEnumerator RunGame()
        {
            while (true)
            {
                yield return new WaitForSeconds(TurnDuration);
                
                listAttackCharacter = attackTeam.GetComponentsInChildren<CharacterController>().ToList();
                
                listDefendCharacter = defendTeam.GetComponentsInChildren<CharacterController>().ToList();

                if (listAttackCharacter.Count == 0 || listDefendCharacter.Count == 0)
                {
                    ShowResultPanel(listAttackCharacter.Count > 0);
                    
                    yield break;
                }
                
                if (_isPause) continue;

                foreach (var character in listAttackCharacter)
                {
                    character.RefreshRandomNumber();
                }
                
                foreach (var character in listDefendCharacter)
                {
                    character.RefreshRandomNumber();
                }

                CheckCharacterAction(false);
                    
                CheckCharacterAction(true);
            }
        }
        
        private void CheckCharacterAction(bool isMove)
        {
            var listCheck = new List<CharacterController>();
            foreach (var character in listAttackCharacter)
            {
                if (character.IsMove == isMove) listCheck.Add(character);
            }
            foreach (var character in listDefendCharacter)
            {
                if (character.IsMove == isMove) listCheck.Add(character);
            }
            
            for (var i = listCheck.Count - 1; i >= 0; i--)
            {
                var character = listCheck[i];
                var characterPosition = character.Position;
                
                var listEnemy = character.IsAttack ? listDefendCharacter : listAttackCharacter;
                
                var enemy = GetClosestEnemy(characterPosition, listEnemy);
                var enemyPosition = enemy.Position;
                
                var distance = Vector2Int.Distance(characterPosition, enemyPosition);
                
                // Check Attack
                if (Mathf.Abs(distance - 1) <= Mathf.Epsilon)
                {
                    enemy.TakeDamage(character.DealDamage(enemy.RandomNumber));
                    continue;
                }
                
                // Check Move
                if(character.IsMove == false) continue;
                
                var listTeam = character.IsAttack ? listAttackCharacter : listDefendCharacter;
                var distanceX = characterPosition.x - enemyPosition.x;
                if (distanceX != 0)
                {
                    var newPosition = new Vector2Int(distanceX > 0 ? characterPosition.x - 1 : characterPosition.x + 1, characterPosition.y);
                    if (CheckPositionValid(newPosition, listTeam))
                    {
                        character.SetPosition(newPosition);
                        continue;
                    }
                }
                    
                var distanceY = characterPosition.y - enemyPosition.y;
                if (distanceY != 0)
                {
                    var newPosition = new Vector2Int(characterPosition.x, distanceY > 0 ? characterPosition.y - 1 : characterPosition.y + 1);
                    if (CheckPositionValid(newPosition, listTeam)) character.SetPosition(newPosition);
                }
            }
        }

        private CharacterController GetClosestEnemy(Vector2Int position, List<CharacterController> listEnemy)
        {
            var distanceMin = -1f;
            CharacterController result = null;
            for (var i = 0; i < listEnemy.Count; i++)
            {
                var character = listEnemy[i];
                if (character == null) continue;

                var enemyPosition = character.Position;
                var distance = Vector2Int.Distance(position, enemyPosition);

                if (distanceMin > 0 && distanceMin <= distance) continue;

                result = character;
                distanceMin = distance;
            }

            return result;
        }

        private bool CheckPositionValid(Vector2Int position, List<CharacterController> listCharacter)
        {
            foreach (var character in listCharacter)
            {
                if (character.Position == position) return false;
            }

            return true;
        }

        #endregion

        #region RESULT

        private void ShowResultPanel(bool isAttackTeamWin)
        {
            var str = isAttackTeamWin ? "Attack Success" : "Defend Success";
            resultDescription.text = str;

            resultPanel.SetActive(true);
        }

        #endregion

        #region BUTTON

        [Header("Button")] 
        public Text speedButtonTxt;
        public Text pauseButtonTxt;
        
        public void HandleClickSpeedButton()
        {
            if (_speed == 1)
            {
                _speed = 2;
                speedButtonTxt.text = "x2";
            }
            else
            {
                _speed = 1;
                speedButtonTxt.text = "x1";
            }
        }
        
        public void HandleClickPauseButton()
        {
            if (_isPause)
            {
                _isPause = false;
                pauseButtonTxt.text = "Pause";
            }
            else
            {
                _isPause = true;
                pauseButtonTxt.text = "Resume";
            }
        }

        public void HandleClickResetButton()
        {
            Init();
        }

        #endregion
    }   
}
