using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Map Data", menuName = "GameAsset/MapData", order = 10)]
public class MapData : ScriptableObject
{
    public float widthSize;
    public float heightSize;
}
